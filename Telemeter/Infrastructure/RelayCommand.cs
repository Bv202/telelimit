﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Telemeter.Infrastructure
{
    using System;
    using System.Diagnostics;
    using System.Windows.Input;
    public class RelayCommand : ICommand
    {
        readonly Func<Boolean> _canExecute;
        readonly Action<Object> _execute;

        public RelayCommand(Action<Object> execute) : this(execute, null)
        {
        }

        public RelayCommand(Action<Object> execute, Func<Boolean> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");
            _execute = execute;
            _canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {

                if (_canExecute != null)
                    CommandManager.RequerySuggested += value;
            }
            remove
            {

                if (_canExecute != null)
                    CommandManager.RequerySuggested -= value;
            }
        }

        [DebuggerStepThrough]
        public Boolean CanExecute(Object parameter)
        {
            return _canExecute == null ? true : _canExecute();
        }

        public void Execute(Object parameter)
        {
            _execute(parameter);
        }
    }
}
