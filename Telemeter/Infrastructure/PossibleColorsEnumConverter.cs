﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Telemeter.ViewModels;

namespace Telemeter.Infrastructure
{
    [ValueConversion(typeof(OverviewViewModel.PossibleColors), typeof(OverviewViewModel.PossibleColors))]
    public class PossibleColorsEnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return "/Images/" + ((OverviewViewModel.PossibleColors)value).ToString().ToLower() + ".ico";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
