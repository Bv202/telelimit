﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Telemeter.Infrastructure
{
    public interface IRequestCloseViewModel
    {
        event Action<bool> RequestClose;
    }
}
