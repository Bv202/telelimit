﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Telemeter.Infrastructure
{
    interface IWindowCloser
    {
        void CloseWindow(bool openOther);
    }
}
