﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;

namespace Telemeter.Infrastructure
{
    class ConfigHelper
    {
        static byte[] entropy = System.Text.Encoding.Unicode.GetBytes("89#dJ9qBP#NHVMfj,");

        public static string EncryptString(string input)
        {
            byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(System.Text.Encoding.Unicode.GetBytes(input), entropy, System.Security.Cryptography.DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedData);
        }

        public static string DecryptString(string encryptedData)
        {
            try
            {
                byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(Convert.FromBase64String(encryptedData), entropy, System.Security.Cryptography.DataProtectionScope.CurrentUser);
                return System.Text.Encoding.Unicode.GetString(decryptedData);
            }
            catch
            {
                return "";
            }
        }

        public static void SaveusernameAndPassword(string username = "", string password = "")
        {
            Properties.Settings.Default.Username = username;
            Properties.Settings.Default.Password = password == "" ? "" : ConfigHelper.EncryptString(password);
            Properties.Settings.Default.Save();
        }

        public static void SaveHours(string color)
        {
            Properties.Settings.Default.Hours = color;
            Properties.Settings.Default.Save();
        }
    }
}
