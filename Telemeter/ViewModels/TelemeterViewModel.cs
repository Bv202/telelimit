﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Telemeter.Infrastructure;
using Telemeter.Models;

namespace Telemeter.ViewModels
{
    public class TelemeterViewModel : ObservableObject, IRequestCloseViewModel
    {
        public TelemeterHandler TelemeterHandler { get; private set; }
        private bool loading;
        private Dispatcher dispatcher;
        private string error;
        public bool Finished { get; private set; }
        public event Action<bool> RequestClose;
        private bool remember;


        public TelemeterViewModel()
        {
            this.TelemeterHandler = new TelemeterHandler();
            this.TelemeterHandler.Callback = this.DisplayResult;

            this.Username = Properties.Settings.Default.Username;
            this.Password = ConfigHelper.DecryptString(Properties.Settings.Default.Password);
            this.Loading = false;
            this.dispatcher = Dispatcher.CurrentDispatcher;
            this.Remember = true;
        }

        public string Username
        {
            get { return TelemeterHandler.Username; }
            set
            {
                if (TelemeterHandler.Username != value)
                {
                    TelemeterHandler.Username = value;
                    RaisePropertyChanged("Username");
                }
            }
        }

        public string Password
        {
            get { return TelemeterHandler.Password; }
            set
            {
                if (TelemeterHandler.Password != value)
                {
                    TelemeterHandler.Password = value;
                    RaisePropertyChanged("Password");
                }
            }
        }

        public bool Loading
        {
            get { return loading; }
            set
            {
                if (loading != value)
                {
                    loading = value;
                    RaisePropertyChanged("Loading");
                }
            }
        }

        public string Error
        {
            get { return error; }
            set
            {
                if (error != value)
                {
                    error = value;
                    RaisePropertyChanged("Error");
                }
            }
        }

        public bool Remember
        {
            get { return remember; }
            set
            {
                if (remember != value)
                {
                    remember = value;
                    RaisePropertyChanged("Remember");
                }
            }
        }

        private void DisplayResult(Exception result)
        {
            Loading = false;

            if (result != null)
            {
                if (result.Message.Contains("ERRTLMTLS_00004") || result.Message.Contains("ERRLNGMGT_00016"))
                {
                    Error = "Ongeldige login!";
                } else
                {
                    Error = "Verbindingsfout";
                }

                dispatcher.Invoke(new Action(CommandManager.InvalidateRequerySuggested), null);
            } else
            {
                SaveUsernameAndPassword();
                Finished = true;
                dispatcher.Invoke(RequestClose, true);
            }
        }

        private void SaveUsernameAndPassword()
        {
            if (Remember)
            {
                ConfigHelper.SaveusernameAndPassword(Username, Password);
            }
            else
            {
                ConfigHelper.SaveusernameAndPassword();
            }
        }

        private void GetTelemeterDataExecute(object passwordbox)
        {
            if (Password.Trim().Length == 0 || Username.Trim().Length == 0)
            {
                Error = "Gelieve alle velden in te vullen!";
            }
            else
            {
                Error = "";
                Loading = true;
                Thread oThread = new Thread(new ThreadStart(TelemeterHandler.GetData));
                oThread.Start();
            }
    }

        public ICommand GetTelemeterData { get { return new RelayCommand(GetTelemeterDataExecute, () => !Loading); } }
    }
}
