﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using Telemeter.Infrastructure;
using Telemeter.Models;

namespace Telemeter.ViewModels
{       
    public enum PossibleHours
    {
        [Description("overdag")]
        RED,
        [Description("'s nachts")]
        GREEN
    };

    class SettingsViewModel : ObservableObject, IRequestCloseViewModel
    {


        public event Action<bool> RequestClose;
        private TelemeterHandler handler;
        private bool remember;
        private bool registry;
        private RegistryKey registryKey;
        private PossibleHours hours;
 
        public SettingsViewModel(TelemeterHandler handler)
        {
            this.handler = handler;
            this.Remember = Properties.Settings.Default.Username.Length > 0 && Properties.Settings.Default.Password.Length > 0 ? true : false;
            this.registryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            this.Registry = registryKey.GetValue("TeleLimit") != null && (registryKey.GetValue("TeleLimit").ToString()).Equals("\"" + System.Reflection.Assembly.GetExecutingAssembly().Location + "\"") ? true : false;
            this.Hours = Properties.Settings.Default.Hours == PossibleHours.GREEN.ToString().ToLower() ? PossibleHours.GREEN : PossibleHours.RED;
        }

        public bool Remember
        {
            set
            {
                if (remember != value)
                {
                    remember = value;
                    RaisePropertyChanged("Remember");
                }
            }
            get { return remember; }
        }

        public bool Registry
        {
            set
            {
                if (registry != value)
                {
                    registry = value;
                    RaisePropertyChanged("Registry");
                }
            }
            get { return registry; }
        }

        public PossibleHours Hours
        {
            set
            {
                if (hours != value)
                {
                    hours = value;
                    RaisePropertyChanged("Hours");
                }
            }
            get { return hours; }
        }

        private void SaveUsernameAndPassword()
        {
            if (Remember)
            {
                ConfigHelper.SaveusernameAndPassword(handler.Username, handler.Password);
            } else
            {
                ConfigHelper.SaveusernameAndPassword();
            }
        }

        private void WriteToRegistry()
        {
            if (Registry)
            {
                registryKey.SetValue("TeleLimit", "\"" + System.Reflection.Assembly.GetExecutingAssembly().Location + "\"");
            } else
            {
                registryKey.DeleteValue("TeleLimit", false);
            }

        }

        private void SaveHours()
        {
            if (Hours == PossibleHours.GREEN)
            {
                ConfigHelper.SaveHours("green");
            }
            else
            {
                ConfigHelper.SaveHours("red");
            }
        }

        private void CloseAppExecute(Object parameter)
        {
            RequestClose(false);
        }

        private void SaveExecute(Object parameter)
        {
            SaveUsernameAndPassword();
            WriteToRegistry();
            SaveHours();
            RequestClose(false);
        }

        public ICommand CloseCommand { get { return new RelayCommand(CloseAppExecute, () => true); } }
        public ICommand SaveCommand { get { return new RelayCommand(SaveExecute, () => true); } }
    }
}
