﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows.Input;
using System.Windows.Threading;
using Telemeter.Infrastructure;
using Telemeter.Models;
using Telemeter.TelenetService;

namespace Telemeter.ViewModels
{
    class OverviewViewModel : ObservableObject, IRequestCloseViewModel
    {
        public enum PossibleColors { Blue, Black, Green, Yellow, Orange, Red }

        public TelemeterHandler Handler { get; private set; }
        public event Action<bool> RequestClose;
        public event Action RequestAbout;
        public event Action RequestSettings;
        public event Action RequestHideTooltip;
        public bool Loading { get; set; }
        private string limitText;
        private Dispatcher dispatcher;
        private PossibleColors currentColor;
        private DateTime LastUpdate { get; set; }
        private Timer timer;

        public OverviewViewModel(TelemeterHandler handler)
        {
            this.Handler = handler;
            this.Loading = false;
            
            this.dispatcher = Dispatcher.CurrentDispatcher;

            if (Handler != null)
            {
                this.UpdateLimitText();
                this.LastUpdate = DateTime.Now;
            }
            else
            {
                this.Handler = new TelemeterHandler { Username = Telemeter.Properties.Settings.Default.Username, Password = ConfigHelper.DecryptString(Telemeter.Properties.Settings.Default.Password) };
            }

            this.Handler.Callback = UpdateResults;
        }

        public string LimitText
        {
            private set
            {
                if (limitText != value)
                {
                    limitText = value;
                    RaisePropertyChanged("LimitText");
                }
            }
            get { return limitText; }
        }

        public PossibleColors CurrentColor
        {
            private set
            {
                if (currentColor != value)
                {
                    currentColor = value;
                    RaisePropertyChanged("CurrentColor");
                }
            }
            get { return currentColor; }
        }

        void TimerTick(object state)
        {
            var now = DateTime.Now;
            if ((now - LastUpdate).TotalMinutes >= 30 && !Loading)
            {
                UpdateExecute(null);
            }
        }

        public void StartTimer()
        {
            this.timer = new Timer(TimerTick, null, TimeSpan.Zero, TimeSpan.FromSeconds(30));
        }

        public void UpdateLimitText()
        {
            if (Handler.Result is VolumeType)
            {
                VolumeType result = (VolumeType)Handler.Result;
                DailyUsageType firstDay = result.DailyUsageList[0];
                DateTime today = DateTime.Today;
                int totalDays = result.DailyUsageList.Length;
                DateTime resetDate = firstDay.Day.AddDays(totalDays);
                TimeSpan diff = resetDate - today;
                string days = diff.Days == 1 ? "1 dag" : diff.Days + " dagen";

                double percent = Math.Round(((double.Parse(result.TotalUsage)/double.Parse(result.Limit))*100), 2);

                if (percent < 50.0)
                {
                    CurrentColor = PossibleColors.Green;
                } else if (percent >= 50.0 && percent < 75.0)
                {
                    CurrentColor = PossibleColors.Yellow;
                } else if (percent >= 75.0 && percent < 100.0)
                {
                    CurrentColor = PossibleColors.Orange;
                } else if (percent >= 100)
                {
                    CurrentColor = PossibleColors.Red;
                } else
                {
                    CurrentColor = PossibleColors.Black;
                }

                LimitText = (double.Parse(result.TotalUsage) / 1024).ToString("F") + "GB (" + percent + "%) van " + (double.Parse(result.Limit) / 1024).ToString("F") + "GB (" + days + " tot reset)";
            }
            else if (Handler.Result is FUPType)
            {
                FUPType fup = (FUPType) Handler.Result;
                DateTime reset = fup.Period.Till.AddDays(1);
                DateTime today = DateTime.Today;
                TimeSpan diff = reset - today;
                string days = diff.Days == 1 ? "1 dag" : diff.Days + " dagen";

                if (fup.Status == StatusType.Grootverbuiker)
                {
                    CurrentColor = PossibleColors.Red;
                    LimitText = "Status: grootverbruiker. Je snelheid wordt verlaagd tijdens de drukke periodes (" + days + "  tot reset)";
                }
                else
                {
                    double percent;
                    if (Properties.Settings.Default.Hours != "green")
                    {
                        percent = Convert.ToDouble((fup.Usage.TotalUsage / fup.Usage.MinUsageRemaining)) * 100;
                    }
                    else
                    {
                        percent = Convert.ToDouble((fup.Usage.TotalUsage / fup.Usage.MaxUsageRemaining)) * 100;

                    }

                    if (percent < 50.0)
                    {
                        CurrentColor = PossibleColors.Green;
                    }
                    else if (percent >= 50.0 && percent < 75.0)
                    {
                        CurrentColor = PossibleColors.Yellow;
                    }
                    else if (percent >= 75.0)
                    {
                        CurrentColor = PossibleColors.Orange;
                    }
                    else
                    {
                        CurrentColor = PossibleColors.Black;
                    }

                    LimitText = fup.Usage.TotalUsage.ToString("F") + "GB van minimum " + fup.Usage.MinUsageRemaining.ToString("F") + "GB (" + Math.Round((Convert.ToDouble((fup.Usage.TotalUsage / fup.Usage.MinUsageRemaining)) * 100), 2) + "%) en maximum " + fup.Usage.MaxUsageRemaining.ToString("F") + "GB (" + Math.Round((Convert.ToDouble((fup.Usage.TotalUsage / fup.Usage.MaxUsageRemaining)) * 100), 2) + "%) (" + days + " tot reset)";
                }
            } else
            {
                CurrentColor = PossibleColors.Black;
                LimitText = "Je abbonementstype wordt nog niet ondersteund door dit programma.";
            }
        }

        private void CloseAppExecute(Object parameter)
        {
            timer.Dispose();
            RequestClose(Convert.ToBoolean(parameter));
        }

        private void UpdateExecute(Object parameter)
        {
            LastUpdate = DateTime.Now;
            Loading = true;
            CurrentColor = PossibleColors.Blue;
            LimitText = "Even geduld, nieuwe gegevens worden binnengehaald...";
            dispatcher.Invoke(new Action(CommandManager.InvalidateRequerySuggested), null);
     
            Thread oThread = new Thread(new ThreadStart(Handler.GetData));
            oThread.Start();
        }

        private void UpdateResults(Exception e)
        {
            dispatcher.Invoke(RequestHideTooltip);
            if (e == null)
            {
                UpdateLimitText();
            } else
            {
                CurrentColor = PossibleColors.Black;
                LimitText = "De gegevens konden niet verkregen worden. Klik op het icoon om het opnieuw te proberen.";
            }

            Loading = false; 
   
            dispatcher.Invoke(new Action(CommandManager.InvalidateRequerySuggested), null);
        }

        private void AboutExecute(Object parameter)
        {
            RequestAbout();
        }

        private void SettingsExecute(Object parameter)
        {
            RequestSettings();
        }

        public ICommand CloseCommand { get { return new RelayCommand(CloseAppExecute, () => true); } }
        public ICommand SettingsCommand { get { return new RelayCommand(SettingsExecute, () => true); } }
        public ICommand UpdateCommand { get { return new RelayCommand(UpdateExecute, () => !Loading); } }
        public ICommand ChangeUserCommand { get { return new RelayCommand(CloseAppExecute, () => !Loading); } }
        public ICommand AboutCommand { get { return new RelayCommand(AboutExecute, () => true); } }
    }
}
