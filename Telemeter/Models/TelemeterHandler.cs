﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telemeter.TelenetService;

namespace Telemeter.Models
{
    public class TelemeterHandler
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public Action<Exception> Callback { get; set; }

        // Bad design but no other solution for now..
        public object Result { get; set; }

        public void GetData()
        {
            try
            {
                TelemeterServiceClient client = new TelemeterServiceClient();
                RetrieveUsageRequestType request = new RetrieveUsageRequestType { UserId = Username, Password = Password };
                RetrieveUsageResponseType response = client.retrieveUsage(request);
                Result = response.Item;

                Callback(null);
            }
            catch (Exception e)
            {
               Result = null;
               Callback(e);
            }
        }
    }
}
