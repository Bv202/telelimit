﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telemeter.Models;
using Telemeter.ViewModels;

namespace Telemeter
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings(TelemeterHandler model)
        {
            InitializeComponent();
            SettingsViewModel viewmodel = new SettingsViewModel(model);
            viewmodel.RequestClose += CloseWindow;

            DataContext = viewmodel;
        }

        public void CloseWindow(bool openOther)
        {
            this.Close();
        }
    }
}
