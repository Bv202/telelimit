﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using Telemeter.Infrastructure;

namespace Telemeter
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            if (Telemeter.Properties.Settings.Default.Username.Trim().Length > 0 && Telemeter.Properties.Settings.Default.Password.Trim().Length > 0)
            {
                Overview window = new Overview();
            } else
            {
                LoginWindow window = new LoginWindow();
                window.Show();
            }
        }
    }
}
