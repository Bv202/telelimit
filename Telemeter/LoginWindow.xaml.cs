﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telemeter.Infrastructure;
using Telemeter.Models;
using Telemeter.TelenetService;
using Telemeter.ViewModels;

namespace Telemeter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window, IWindowCloser
    {
        public LoginWindow()
        {
            InitializeComponent();

            var viewmodel = new TelemeterViewModel();
            viewmodel.RequestClose += CloseWindow;
            this.DataContext = viewmodel;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TelemeterViewModel vm = (TelemeterViewModel) this.DataContext;

            if (vm.Finished != true)
            {
                if ( MessageBox.Show("Bent u zeker dat u TeleLimit wilt afsluiten?", "Afsluiten?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        public void CloseWindow(bool openOther)
        {
            if (openOther)
            {
                TelemeterViewModel vm = (TelemeterViewModel) this.DataContext;
                Overview overview = new Overview(vm.TelemeterHandler);
            }

            this.Close();
        }
    }
}
