﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telemeter.Infrastructure;
using Telemeter.Models;
using Telemeter.ViewModels;

namespace Telemeter
{
    /// <summary>
    /// Interaction logic for Overview.xaml
    /// </summary>
    public partial class Overview : Window, IWindowCloser
    {
        private About about;
        private Settings settings;

        public Overview() :
            this (null)
        {
        }

        public Overview(TelemeterHandler model)
        {
            var viewmodel = new OverviewViewModel(model);
            this.DataContext = viewmodel;
            viewmodel.RequestClose += CloseWindow;
            viewmodel.RequestAbout += OpenAbout;
            viewmodel.RequestSettings += OpenSettings;
            viewmodel.RequestHideTooltip += HideTooltip;
            InitializeComponent();

            viewmodel.StartTimer();
        }

        public void OpenSettings()
        {
            if (settings == null || !settings.IsVisible)
            {
                settings = new Settings(((OverviewViewModel)DataContext).Handler);
                settings.Closed += UpdateIcon;
                settings.ShowDialog();
            }
        }

        public void OpenAbout()
        {
            if (about == null || !about.IsVisible)
            {
                about = new About();
                about.ShowDialog();
            }
        }

        public void UpdateIcon(Object o, EventArgs args)
        {
            OverviewViewModel model = (OverviewViewModel) DataContext;

            if (!model.Loading)
            {
                model.UpdateLimitText();
            }
        }

        public void CloseWindow(bool openOther)
        {
            if (openOther)
            {
                LoginWindow login = new LoginWindow();
                login.Show();
            }

            if (about != null && about.IsVisible)
            {
                about.Close();
            }

            if (settings != null && settings.IsVisible)
            {
                settings.Close();
            }

            Icon.Dispose();
            Close();
        }

        public void HideTooltip()
        {
            Icon.OnToolTipChange(false);
        }
    }
}
